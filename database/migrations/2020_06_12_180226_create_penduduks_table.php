<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenduduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penduduk', function (Blueprint $table) {
            $table->id();
            $table->string('no_nik');
            $table->bigInteger('id_kk');
            $table->bigInteger('id_pendukung');
            $table->bigInteger('id_jabatan');
            $table->string('nama_lengkap');
            $table->integer('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->string('agama');
            $table->string('pendidikan');
            $table->string('jenis_pekerjaan');
            $table->string('status_pernikahan');
            $table->string('status_hubungan_dalam_keluarga');
            $table->string('kewarganegaraan');
            $table->string('no_paspor');
            $table->string('no_kitas_kitap');
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penduduk');
    }
}
