<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('datakeluarga/', 'KartuKeluargaController@show');
Route::get('datajabatan/', 'JabatanController@show');

Route::post('checkNIK/', 'api\AuthController@checkNIK');
Route::post('addFcmToken/', 'api\AuthController@addFcmToken');
Route::post('SendNotif/', 'api\SendPesanController@sendNotif');

Route::get('listSurat/', 'api\ListMenuController@listSurat');
Route::post('requestSurat/', 'api\ListMenuController@requestSurat');
Route::post('history/', 'api\ListMenuController@history');
Route::post('accSurat/', 'api\ListMenuController@accSurat');

Route::post('getPesan/', 'api\PesanMasukController@getPesan');
Route::post('statusPesan/', 'api\PesanMasukController@statusPesan');
Route::post('deletePesan/', 'api\PesanMasukController@deletePesan');

Route::post('getProfile/', 'api\profileController@getProfile');