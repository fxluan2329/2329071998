<style>
    .serif {
        font-family: "Times New Roman", Times, serif;
    }

    .Bold {
        font-weight: bold;
        font-size: 20px;
    }

    .Normal {
        font-weight: normal;
        font-size: 12;
    }

    .underline {
        text-decoration: underline;
    }

    table,
    td {
        border: 0px solid black;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    td {
        vertical-align: bottom;
        font-weight: normal;
        font-size: 12;
    }

    tr {
        vertical-align: bottom;
    }
</style>

<div class="container">
    <table>
        <tr>
            <td style="vertical-align: middle; width: 15%;text-align: right">
                <img src="http://localhost:8000/storage/surat/kop_surat.jpg" alt="" sizes="" srcset="" height="130" width="120">
            </td>
            <td>
                <p class="text-center serif Bold">PEMERINTAHAN KABUPATEN LAMONGAN<br />KECAMATAN DEKET<br />DESA DEKET KULON</p>
                <p class="text-center serif Normal">Alamat : Jalan Raya Deket No 60 Deket Kulon Tlp . (0322)123456<br />Kode Pos 62291 Deket Kulon</p>
            </td>
        </tr>

    </table>
    <div class="col-sm-12" style="border-style: double; margin-bottom: 12px"></div>
</div>