<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SKTM || PREVIEW</title>
</head>

<body>
    <button onclick="goBack()">Kembali</button>
    <button onclick="printDiv('printableArea')">Cetak
    </button>
    <div id="printableArea" class="container">
        @include('templateSurat.kopSurat')
        @foreach($dataSurat as $datasurat)
        <!-- TITLE SURAT -->
        <p class="text-center underline Bold" style="font-size: 14px;">SURAT KETERANGAN TIDAK MAMPU</p>
        <p class="text-center Bold " style="font-size: 14px;">Nomor : 465/.......................................</p>

        <!-- BODY SURAT -->

        <table>
            <tr style="height: 35px;">
                <td style="width: 10%;">
                </td>
                <td style="width: 90%;" colspan="4" class="serif Normal">
                    Yang bertandatangan dibawah ini Kepala Desa Deket Kulon, Kecamatan Deket, Kabupaten Lamongan menerangkan dengan sebenarnya bahwa :
                </td>
            </tr>
            @foreach($dataAyah as $dataayah)
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Nama (Ayah)
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataayah->nama_lengkap ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Tempat, Tgl.Lahir
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataayah->tempat_lahir . '. ' . $dataayah->tanggal_lahir ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Agama
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataayah->agama ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Pekerjaan
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataayah->jenis_pekerjaan ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Alamat
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataayah->alamat . ' ' . $dataayah->rt_rw ?>
                </td>
            </tr>
            @endforeach
            @foreach($dataIbu as $dataibu)
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Nama (Ibu)
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataibu->nama_lengkap ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Tempat, Tgl.Lahir
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataibu->tempat_lahir . ', ' . $dataibu->tanggal_lahir ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Agama
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataibu->agama ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Pekerjaan
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataibu->jenis_pekerjaan ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Alamat
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $dataibu->alamat . ', ' . $dataibu->rt_rw ?>
                </td>
            </tr>
            @endforeach
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    No KK
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $datasurat->no_kk ?>
                </td>
            </tr>
            <tr style="height: 35px;">
                <td style="width: 10%;">
                </td>
                <td style="width: 90%;" colspan="4" class="serif Normal">
                    Adalah benar orang tua dari :
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Nama
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $datasurat->nama_lengkap ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Tempat, Tgl.Lahir
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $datasurat->tempat_lahir . ", " . $datasurat->tanggal_lahir ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Agama
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $datasurat->agama ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Pekerjaan
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    <?= $datasurat->jenis_pekerjaan ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;" class="serif Normal">
                    Sekolah / Kuliah
                </td>
                <td style="width: 2%;" class="serif Normal">
                    :
                </td>
                <td style="width: 75%;" colspan="2" class="serif Normal">
                    Parameter
                </td>
            </tr>
            <tr style="height: 35px;">
                <td style="width: 10%;">
                </td>
                <td style="width: 90%;" colspan="4" class="serif Normal">
                    Yang bersangkutan diatas adalah benar - benar Penduduk kami dari <strong>Keluarga Tidak Mampu.</strong>
                </td>
            </tr>
            <tr>
                <td style="width: 10%;">
                </td>
                <td style="width: 90%;" colspan="4" class="serif Normal">
                    Demikian surat keterangan ini kami buat agar dapat dipergunakan untuk syarat <?= $datasurat->keperluan ?>
                </td>
            </tr>
            <tr style="height: 35px;">
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;">
                </td>
                <td style="width: 2%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 35%;" class="serif Normal text-center">
                    Deket Kulon , <?= date("d-m-Y") ?>
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;">
                </td>
                <td style="width: 2%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 35%;" class="serif Normal text-center">
                    KEPALA DESA DEKET KULON
                </td>
            </tr>
            <tr style="height: 80px;">
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;">
                </td>
                <td style="width: 2%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 35%;" class="serif Normal text-center">
                </td>
            </tr>
            <tr>
                <td style="width: 8%;">
                </td>
                <td style="width: 15%;">
                </td>
                <td style="width: 2%;">
                </td>
                <td style="width: 35%;">
                </td>
                <td style="width: 35%;" class="serif Normal text-center">
                    <?= $datasurat->kepala_desa ?>
                </td>
            </tr>
        </table>
        @endforeach
</body>

</html>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }

    function goBack() {
        window.history.back();
    }
</script>