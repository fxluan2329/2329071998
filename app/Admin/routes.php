<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    //VIEW HOME
    $router->get('/', 'HomeController@index')->name('home');

    //VIEW KELUARGA
    $router->get('/Keluarga', 'KartuKeluargaController@index')->name('Data Keluarga');
    $router->get('/Keluarga/create', 'KartuKeluargaController@create')->name('Data Keluarga');
    $router->get('/Keluarga/{id}/edit', 'KartuKeluargaController@edit')->name('Data Keluarga');
    $router->post('/Keluarga/save', 'KartuKeluargaController@save');
    $router->post('/Keluarga/update', 'KartuKeluargaController@update');

    //VIEW PENDUDUK
    $router->get('/Penduduk', 'PendudukController@index')->name('Data Penduduk');
    $router->get('/Penduduk/create', 'PendudukController@create')->name('Data Penduduk');
    $router->get('/Penduduk/{id}/edit', 'PendudukController@edit')->name('Data Penduduk');
    $router->post('/Penduduk/save', 'PendudukController@save');
    $router->post('/Penduduk/update', 'PendudukController@update');

    //VIEW DATA SURAT
    $router->get('/DataSurat', 'DataSuratController@index')->name('Data Surat');

    //VIEW DATA INSTANSI
    $router->get('/ProfilInstansi', 'InstansiController@index')->name('Data Instansi');


    //VIEW DATA JABATAN
    $router->get('/DataJabatan', 'DataJabatanController@index')->name('Data Jabatan');
    $router->get('/DataJabatan/create', 'DataJabatanController@create')->name('Data Jabatan');
    $router->get('/DataJabatan/{id}/edit', 'DataJabatanController@edit')->name('Data Jabatan');
    $router->post('/DataJabatan/save', 'DataJabatanController@save');
    $router->post('/DataJabatan/update', 'DataJabatanController@update');

    //VIEW PENGAJUAN
    $router->get('/PengajuanSurat', 'PengajuanSuratController@index')->name('Data Pengajuan Surat');
    $router->get('/PengajuanSurat/{id}/Preview', 'PengajuanSuratController@preview')->name('Data Pengajuan Surat');
});
