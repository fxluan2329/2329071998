<?php

namespace App\Admin\Controllers;

use App\KartuKeluarga;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class KartuKeluargaController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data Keluarga')
            ->row($this->grid());
    }

    public function create(Content $content)
    {
        return $content
            ->title('Tambah Data Keluarga')
            ->body($this->formCreate());
    }

    public function save(Request $request)
    {
        $no_kk   = $request->post('no_kk');
        $alamat  = $request->post('alamat');
        $rt_rw   = $request->post('rt') . "/" . $request->post('rw');

        $result = DB::table('kartu_keluarga')->insert([
            'no_kk' => $no_kk,
            'alamat' => $alamat,
            'rt_rw' => $rt_rw
        ]);

        return redirect('/admin/Keluarga');
    }

    public function edit(Content $content, Request $request)
    {
        return $content
            ->title('Edit Data Keluarga')
            ->body($this->formUpdate($request));
    }

    public function update(Request $request)
    {
        $id   = $request->post('id');
        $no_kk   = $request->post('no_kk');
        $alamat  = $request->post('alamat');
        $rt_rw   = $request->post('rt') . "/" . $request->post('rw');

        $result = DB::table('kartu_keluarga')
            ->where('id', $id)
            ->update([
                'no_kk' => $no_kk,
                'alamat' => $alamat,
                'rt_rw' => $rt_rw
            ]);

        if ($result) {
            return redirect('/admin/Keluarga');
        } else {
            return $result;
        }
    }

    protected function grid()
    {
        $grid = new Grid(new KartuKeluarga);

        $grid->column('no_kk', __('NO KK'))->sortable();
        $grid->column('kepala_keluarga', __('KEPALA KELUARGA'))->sortable();
        $grid->column('jml_agt_Kel', __('ANGGOTA KELUARGA'))->sortable();
        $grid->column('ALAMAT')->display(function () {
            return $this->alamat . ', ' . $this->rt_rw;
        });


        return $grid;
    }

    protected function formCreate()
    {

        $form = new Form(new KartuKeluarga());
        $form->text('no_kk', 'No KK');
        $form->text('alamat', 'Alamat');
        $form->text('rt', 'RT');
        $form->text('rw', 'RW');
        $form->setAction('/admin/Keluarga/save');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();
            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();
            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });
        return $form;
    }

    protected function formUpdate($request)
    {

        $value = KartuKeluarga::find($request->id);
        $form = new Form(new KartuKeluarga());

        $form = new Form(new KartuKeluarga());
        $form->html('<input type="hidden" name="id" id="id" value="' . $request->id . '">');
        $form->text('no_kk', 'No KK')->value($value->no_kk);
        $form->text('alamat', 'Alamat')->value($value->alamat);
        $rt_rw = explode("/", $value->rt_rw);
        $form->text('rt', 'RT')->value($rt_rw[0]);
        $form->text('rw', 'RW')->value($rt_rw[1]);
        $form->setAction('/admin/Keluarga/update');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();
            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();
            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
