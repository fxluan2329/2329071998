<?php

namespace App\Admin\Controllers;

use App\data_instansi;
use Encore\Admin\Grid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class InstansiController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data Instansi')
            ->row($this->content());
    }

    protected function content()
    {
        return view('admin.dataPenduduk');
    }
}
