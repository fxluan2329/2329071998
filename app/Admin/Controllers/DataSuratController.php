<?php

namespace App\Admin\Controllers;

use App\data_surat;
use Encore\Admin\Grid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class DataSuratController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data List Surat')
            ->row($this->grid());
    }

    protected function grid()
    {
        $grid = new Grid(new data_surat);

        $grid->column('kode_produk', __('KODE PRODUK'))->sortable();
        $grid->column('nama_produk', __('NAMA SURAT'))->sortable();
        $grid->column('isActive')->display(function ($ss) {
            return $ss ? 'Aktif' : 'Tidak Aktif';
        })->sortable();

        return $grid;
    }
}
