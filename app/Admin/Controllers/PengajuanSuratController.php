<?php

namespace App\Admin\Controllers;

use App\status_surat;
use DB;
use Encore\Admin\Grid;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class PengajuanSuratController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data Pemohon Surat')
            ->row($this->grid());
    }

    public function preview(Content $content, Request $request)
    {
        $id = $request->id;
        $dataSurat = DB::table('v_status_surat')
            ->where('id', $id)
            ->get();
        $dataAyah = DB::table('v_penduduk')
            ->where('no_kk', $dataSurat[0]->no_kk)
            ->where('nama_lengkap', $dataSurat[0]->nama_ayah)
            ->where('status_hubungan_dalam_keluarga', 'kepala keluarga')

            ->get();

        $dataIbu = DB::table('v_penduduk')
            ->where('no_kk', $dataSurat[0]->no_kk)
            ->where('nama_lengkap', $dataSurat[0]->nama_ibu)
            ->where('status_hubungan_dalam_keluarga', 'istri')
            ->get();

        // $datas['surat'] = $dataSurat[0];
        // $datas['ayah'] = $dataAyah[0];
        // $datas['ibu'] = $dataIbu[0];

        // dump($datas);
        // echo $datas['surat']->keperluan;
        return $content
            ->title('Preview Surat')
            ->row(view('templateSurat/SKTMP', compact('dataSurat', 'dataAyah', 'dataIbu')));
    }

    protected function grid()
    {
        $grid = new Grid(new status_surat);
        $grid->disableCreateButton();
        $grid->disableActions();

        $grid->column('id', __('ID PENGAJUAN'))->sortable();
        $grid->column('nama_produk', __('NAMA SURAT'))->sortable();
        $grid->column('nama_lengkap', __('NAMA PEMOHON'))->sortable();
        $grid->column('konfirmasi_rt')->display(function ($rt) {
            return $rt == 2 ? 'Ditolak' : ($rt ? 'Disetujui' : 'Belum Disetujui');
        })->sortable();
        $grid->column('konfirmasi_rw')->display(function ($rw) {
            return $rw == 2 ? 'Ditolak' : ($rw ? 'Disetujui' : 'Belum Disetujui');
        })->sortable();
        $grid->column('keterangan', __('KETERANGAN'))->sortable();
        $grid->column('ALAMAT')->display(function () {
            return $this->alamat . ', ' . $this->rt_rw;
        });
        $grid->column('Action')->display(function () {
            return '
            <div class="btn-group" role="group" aria-label="Basic example">
                <a href="PengajuanSurat/' . $this->id . '/Preview"><button type="button" class="btn btn-primary">Preview</button></a>
            </div>
            ';
        });


        return $grid;
    }

    public function generate_pdf()
    {
        $data = [
            'foo' => 'bar'
        ];
        $pdf = PDF::loadView('pdf.document', $data);
        return $pdf->stream('document.pdf');
    }
}
