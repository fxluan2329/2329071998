<?php

namespace App\Admin\Controllers;

use App\jabatan;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class DataJabatanController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data List Jabatan')
            ->row($this->grid());
    }

    public function create(Content $content)
    {
        return $content
            ->title('Tambah List Jabatan')
            ->body($this->formCreate());
    }

    public function edit(Content $content, Request $request)
    {
        return $content
            ->title('Edit List Jabatan')
            ->body($this->formUpdate($request));
    }

    public function save(Request $request)
    {
        $nama_jabatan        = $request->post('nama_jabatan');

        $result = DB::table('jabatan')->insert([
            'nama_jabatan' => $nama_jabatan
        ]);

        return redirect('/admin/DataJabatan');
    }

    public function update(Request $request)
    {
        $id                  = $request->post('id');
        $nama_jabatan        = $request->post('nama_jabatan');

        $result = DB::table('jabatan')
            ->where('id', $id)
            ->update([
                'nama_jabatan' => $nama_jabatan,
            ]);
        return redirect('/admin/DataJabatan');
    }

    protected function grid()
    {
        $grid = new Grid(new jabatan);

        $grid->column('id', __('ID JABATAN'))->sortable();
        $grid->column('nama_jabatan', __('NAMA JABATAN'))->sortable();
        $grid->actions(function ($actions) {
            $actions->disableView();
            // $actions->disableEdit();
            $actions->getKey();
            $actions->append('<a href=""><i class="fa fa-eye">teststs</i></a>');
        });
        return $grid;
    }

    protected function formCreate()
    {

        $form = new Form(new jabatan);
        $form->display('id', 'ID');
        $form->text('nama_jabatan', 'Nama Jabatan');
        $form->setAction('/admin/DataJabatan/save');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();
            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();
            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });
        return $form;
    }

    protected function formUpdate($request)
    {
        $value = jabatan::find($request->id);
        $form = new Form(new jabatan);
        $form->html('<input type="hidden" name="id" id="id" value="' . $request->id . '">');
        $form->text('nama_jabatan', 'Nama Jabatan')->value($value->nama_jabatan);
        $form->setAction('/admin/DataJabatan/update');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            // disable `View` checkbox
            $footer->disableViewCheck();
            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();
            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });

        return $form;
    }
}
