<?php

namespace App\Admin\Controllers;

use App\Penduduk;
use App\KartuKeluarga;
use App\jabatan;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class PendudukController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Data Penduduk')
            ->row($this->grid());
    }

    public function create(Content $content)
    {
        return $content
            ->title('Tambah Penduduk')
            ->body($this->formCreate());
    }

    public function save(Request $request)
    {
        $no_nik                                = $request->post('no_nik');
        $id_kk                                 = $request->post('id_kk');
        $id_jabatan                            = $request->post('id_jabatan');
        $nama_lengkap                          = $request->post('nama_lengkap');
        $jenis_kelamin                         = $request->post('jenis_kelamin');
        $tempat_lahir                          = $request->post('tempat_lahir');
        $tanggal_lahir                          = $request->post('tanggal_lahir');
        $agama                                 = $request->post('agama');
        $pendidikan                            = $request->post('pendidikan');
        $jenis_pekerjaan                       = $request->post('jenis_pekerjaan');
        $status_pernikahan                     = $request->post('status_pernikahan');
        $status_hubungan_dalam_keluarga        = $request->post('status_hubungan_dalam_keluarga');
        $kewarganegaraan                       = $request->post('kewarganegaraan');
        $no_paspor                             = $request->post('no_paspor');
        $no_kitas_kitap                        = $request->post('no_kitas_kitap');
        $nama_ayah                             = $request->post('nama_ayah');
        $nama_ibu                              = $request->post('nama_ibu');

        $result = DB::table('penduduk')->insert([
            'no_nik' => $no_nik,
            'id_kk' => $id_kk,
            'id_jabatan' => $id_jabatan,
            'nama_lengkap' => $nama_lengkap,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'agama' => $agama,
            'pendidikan' => $pendidikan,
            'jenis_pekerjaan' => $jenis_pekerjaan,
            'status_pernikahan' => $status_pernikahan,
            'status_hubungan_dalam_keluarga' => $status_hubungan_dalam_keluarga,
            'kewarganegaraan' => $kewarganegaraan,
            'no_paspor' => $no_paspor,
            'no_kitas_kitap' => $no_kitas_kitap,
            'nama_ayah' => $nama_ayah,
            'nama_ibu' => $nama_ibu,
        ]);

        return redirect('/admin/Penduduk');

        // return $request;
    }

    public function edit(Content $content, Request $request)
    {
        return $content
            ->title('Edit Penduduk')
            ->body($this->formUpdate($request));
    }

    public function update(Request $request)
    {
        $id                                    = $request->post('id');
        $no_nik                                = $request->post('no_nik');
        $id_kk                                 = $request->post('id_kk');
        $id_jabatan                            = $request->post('id_jabatan');
        $nama_lengkap                          = $request->post('nama_lengkap');
        $jenis_kelamin                         = $request->post('jenis_kelamin');
        $tempat_lahir                          = $request->post('tempat_lahir');
        $tanggal_lahir                          = $request->post('tanggal_lahir');
        $agama                                 = $request->post('agama');
        $pendidikan                            = $request->post('pendidikan');
        $jenis_pekerjaan                       = $request->post('jenis_pekerjaan');
        $status_pernikahan                     = $request->post('status_pernikahan');
        $status_hubungan_dalam_keluarga        = $request->post('status_hubungan_dalam_keluarga');
        $kewarganegaraan                       = $request->post('kewarganegaraan');
        $no_paspor                             = $request->post('no_paspor');
        $no_kitas_kitap                        = $request->post('no_kitas_kitap');
        $nama_ayah                             = $request->post('nama_ayah');
        $nama_ibu                              = $request->post('nama_ibu');

        $result = DB::table('penduduk')
            ->where('id', $id)
            ->update([
                'no_nik' => $no_nik,
                'id_kk' => $id_kk,
                'id_jabatan' => $id_jabatan,
                'nama_lengkap' => $nama_lengkap,
                'jenis_kelamin' => $jenis_kelamin,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'agama' => $agama,
                'pendidikan' => $pendidikan,
                'jenis_pekerjaan' => $jenis_pekerjaan,
                'status_pernikahan' => $status_pernikahan,
                'status_hubungan_dalam_keluarga' => $status_hubungan_dalam_keluarga,
                'kewarganegaraan' => $kewarganegaraan,
                'no_paspor' => $no_paspor,
                'no_kitas_kitap' => $no_kitas_kitap,
                'nama_ayah' => $nama_ayah,
                'nama_ibu' => $nama_ibu,
            ]);
        return redirect('/admin/Penduduk');
    }

    protected function grid()
    {
        $grid = new Grid(new Penduduk);
        $grid->quickSearch(function ($model, $query) {
            $model->where('nama_lengkap', 'like', "%{$query}%")->orWhere('nama_jabatan', 'like', "%{$query}%");
        });
        $grid->column('id', __('ID'))->sortable();
        $grid->column('no_nik', __('NO NIK'))->sortable();
        $grid->column('no_kk', __('NO KK'))->sortable();
        $grid->column('nama_lengkap', __('NAMA'))->sortable();
        $grid->column('nama_jabatan', __('STATUS PENDUDUK'))->sortable();
        $grid->column('jenis_kelamin')->display(function ($jk) {
            return $jk ? 'Laki - laki' : 'Perempuan';
        })->sortable();
        $grid->column('tempat_lahir', __('TEMPAT LAHIR'))->sortable();
        $grid->column('agama', __('AGAMA'))->sortable();
        $grid->column('pendidikan', __('PENDIDIKAN'))->sortable();
        $grid->column('jenis_pekerjaan', __('JENIS PEKERJAAN'))->sortable();
        $grid->column('status_pernikahan', __('STATUS PERNIKAHAN'))->sortable();
        $grid->column('status_hubungan_dalam_keluarga', __('HUBUNGAN DALAM KELUARGA'))->sortable();
        $grid->column('kewarganegaraan', __('KEWARGANEGARAAN'))->sortable();
        $grid->column('no_paspor', __('NO PASPOR'))->sortable();
        $grid->column('no_kitas_kitap', __('NO KITAP KITAS'))->sortable();
        $grid->column('nama_ayah', __('NAMA AYAH'))->sortable();
        $grid->column('nama_ibu', __('NAMA IBU'))->sortable();

        return $grid;
    }

    protected function formCreate()
    {
        $form = new Form(new Penduduk);
        $form->setAction('/admin/Penduduk/save');
        $form->text('no_nik', 'No NIK');
        $kk = KartuKeluarga::all();
        $arrKK = array();
        foreach ($kk as $a) {
            $arrKK[$a->id] = $a->no_kk . " ( " . $a->kepala_keluarga . " )";
        }
        $form->select('id_kk', 'No KK')->options($arrKK);

        $jabatan = jabatan::all();
        $arrJabatan = array();
        foreach ($jabatan as $a) {
            $arrJabatan[$a->id] = $a->nama_jabatan;
        }
        $form->select('id_jabatan', 'Jabatan')->options($arrJabatan);
        $form->text('nama_lengkap', 'Nama Lengkap');
        $jk = [
            0  => 'Perempuan',
            1 => 'Laki - Laki',
        ];
        $form->select('jenis_kelamin', 'Jenis Kelamin')->options($jk);
        $form->text('tempat_lahir', 'Tempat Lahir');
        $form->text('tanggal_lahir', 'Tanggal Lahir');
        $agama = [
            'Islam'  => 'Islam',
            'Kristen'  => 'Kristen',
            'Hindu'  => 'Hindu',
            'Budha'  => 'Budha',
        ];
        $form->select('agama', 'Agama')->options($agama);
        $pendidikan = [
            'SD'  => 'SD',
            'SMP'  => 'SMP',
            'SMA'  => 'SMA',
            'D1'  => 'D1',
            'D2'  => 'D2',
            'D3'  => 'D3',
            'Sarjana'  => 'Sarjana',
        ];
        $form->select('pendidikan', 'Pendidikan')->options($pendidikan);
        $form->text('jenis_pekerjaan', 'Jenis Pekerjaan');
        $form->text('status_pernikahan', 'Status Pernikahan');
        $shdk = [
            'Anak' => 'Anak',
            'Kepala Kelurga' => 'Kepala Kelurga',
            'Istri' => 'Istri',
        ];
        $form->select('status_hubungan_dalam_keluarga', 'Status Hubungan Keluarga')->options($shdk);
        $form->text('kewarganegaraan', 'Kewarganegaraan');
        $form->text('no_paspor', 'No Paspor');
        $form->text('no_kitas_kitap', 'No Kitab / Kitas');
        $form->text('nama_ayah', 'Nama Ayah');
        $form->text('nama_ibu', 'Nama Ibu');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {

            // disable `View` checkbox
            $footer->disableViewCheck();

            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();

            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });
        return $form;
    }

    protected function formUpdate($request)
    {
        $form = new Form(new Penduduk);

        $value = Penduduk::find($request->id);

        $form = new Form(new jabatan);
        $form->html('<input type="hidden" name="id" id="id" value="' . $request->id . '">');
        $form->text('no_nik', 'No NIK')->value($value->no_nik);
        $kk = KartuKeluarga::all();
        $arrKK = array();
        foreach ($kk as $a) {
            $arrKK[$a->id] = $a->no_kk . " ( " . $a->kepala_keluarga . " )";
        }
        $form->select('id_kk', 'No KK')->options($arrKK)->value($value->id_kk);

        $jabatan = jabatan::all();
        $arrJabatan = array();
        // $arrJabatan['val'] =
        foreach ($jabatan as $a) {
            $arrJabatan[$a->id] = $a->nama_jabatan;
        }
        $form->select('id_jabatan', 'Jabatan')->options($arrJabatan)->value($value->id_jabatan);
        $form->text('nama_lengkap', 'Nama Lengkap')->value($value->nama_lengkap);
        $jk = [
            0  => 'Perempuan',
            1 => 'Laki - Laki',
        ];
        $form->select('jenis_kelamin', 'Jenis Kelamin')->options($jk)->value($value->jenis_kelamin);
        $form->text('tempat_lahir', 'Tempat Lahir')->value($value->tempat_lahir);
        $form->text('tanggal_lahir', 'Tanggal Lahir')->value($value->tanggal_lahir);
        $agama = [
            'Islam'  => 'Islam',
            'Kristen'  => 'Kristen',
            'Hindu'  => 'Hindu',
            'Budha'  => 'Budha',
        ];
        $form->select('agama', 'Agama')->options($agama)->value($value->agama);
        $pendidikan = [
            'SD'  => 'SD',
            'SMP'  => 'SMP',
            'SMA'  => 'SMA',
            'D1'  => 'D1',
            'D2'  => 'D2',
            'D3'  => 'D3',
            'Sarjana'  => 'Sarjana',
        ];
        $form->select('pendidikan', 'Pendidikan')->options($pendidikan)->value($value->pendidikan);
        $form->text('jenis_pekerjaan', 'Jenis Pekerjaan')->value($value->jenis_pekerjaan);
        $form->text('status_pernikahan', 'Status Pernikahan')->value($value->status_pernikahan);
        $shdk = [
            'Anak' => 'Anak',
            'Kepala Kelurga' => 'Kepala Kelurga',
            'Istri' => 'Istri',
        ];
        $form->select('status_hubungan_dalam_keluarga', 'Status Hubungan Keluarga')->options($shdk)->value($value->status_hubungan_dalam_keluarga);
        $form->text('kewarganegaraan', 'Kewarganegaraan')->value($value->kewarganegaraan);
        $form->text('no_paspor', 'No Paspor')->value($value->no_paspor);
        $form->text('no_kitas_kitap', 'No Kitab / Kitas')->value($value->no_kitas_kitap);
        $form->text('nama_ayah', 'Nama Ayah')->value($value->nama_ayah);
        $form->text('nama_ibu', 'Nama Ibu')->value($value->nama_ibu);
        $form->setAction('/admin/Penduduk/update');
        $form->tools(function (Form\Tools $tools) {
            // Disable `List` btn.
            $tools->disableList();

            // Disable `Delete` btn.
            $tools->disableDelete();

            // Disable `Veiw` btn.
            $tools->disableView();
        });
        $form->footer(function ($footer) {

            // disable `View` checkbox
            $footer->disableViewCheck();

            // disable `Continue editing` checkbox
            $footer->disableEditingCheck();

            // disable `Continue Creating` checkbox
            $footer->disableCreatingCheck();
        });
        return $form;
    }
}
