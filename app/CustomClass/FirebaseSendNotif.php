<?php

namespace App\CustomClass;

class FirebaseSendNotif
{
    public static function  send_notification($registatoin_ids, $data, $device_type)
    {
        try {
            $url = 'https://fcm.googleapis.com/fcm/send';

                $fields = array(
                    'to' => $registatoin_ids,
                    'notification' => $data
                );

            // Firebase API Key
            $headers = array('Authorization:key=AAAAAtFpcKY:APA91bF7Lcxl5_fTuRAr6L9I5QKXWvdoSv4pX4HcDFcOL4U0b7fJnVK0UH34J4k1Jgdk799OZZR3nZAoQg4TljVhIwOhPQBRwWJZ4HQqRgJB6WBSYGKByaTDUf2sc2jRcIeLxMcxegY7', 'Content-Type:application/json');
            // Open connection
            $ch = curl_init();
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
        } catch (\Illuminate\Database\QueryException $ex) {
            //dd ($ex->getMessage());
        }
    }
}
