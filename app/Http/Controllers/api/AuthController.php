<?php

namespace App\Http\Controllers\api;

use App\penduduk;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function checkNIK(Request $request)
    {
        $nik = $request->input('nik');
        $device = $request->input('device');

        $user = DB::table('v_penduduk')->where('no_nik', $nik)->exists();

        if ($user) {
            $users = DB::table('v_penduduk')->where('no_nik', $nik)->get();
            $fcmToken = DB::table('pendukung')->select('fcm_token', 'device_name')->where('id_penduduk', $users[0]->id)->get();
            return response()->json([
                'success' => true,
                'messege' => 'NIK anda terdaftar',
                'data' => array(
                    'id_penduduk' => $users[0]->id,
                    'no_nik' => $users[0]->no_nik,
                    'nama' => $users[0]->nama_lengkap,
                    'jabatan' => $users[0]->nama_jabatan,
                    'fcmToken' => $fcmToken[0]->fcm_token != '0' && $fcmToken[0]->device_name == $device ? true : false
                )
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'messege' => 'NIK anda tidak terdaftar'
            ], 200);
        }
    }

    public function addFcmToken(Request $request)
    {
        $fcm_token = $request->input('fcm_token');
        $nik = $request->input('nik');
        $os = $request->input('os');
        $device = $request->input('device');

        $user = DB::table('v_penduduk')->where('no_nik', $nik)->get();

        $affected = DB::table('pendukung')
            ->where('id_penduduk', $user[0]->id)
            ->update(['fcm_token' => $fcm_token, 'os' => $os, 'device_name' => $device]);

        if ($affected) {
            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }
}
