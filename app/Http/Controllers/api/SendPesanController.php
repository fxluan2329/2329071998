<?php

namespace App\Http\Controllers\api;

use App\penduduk;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomClass\FirebaseSendNotif;

class SendPesanController extends Controller
{
    public function sendNotif(Request $request)
    {
        $id = $request->input('id');
        $pesan = $request->input('pesan');
        $type = $request->input('type');
        $title = $request->input('title');

        $user = DB::table('pendukung')->select('fcm_token', 'os')->where('id_penduduk', $id)->get();

        if ($user) {

            $data = array(
                'title' => 'Admin@Appeldesa',
                'body' => $pesan
                
            );

            FirebaseSendNotif::send_notification($user[0]->fcm_token, $data, $user[0]->os);
            
            DB::table('pesan_masuk')->insert([
                ['id_penduduk' => $id, 'body_pesan' => $pesan, 'title_pesan'=> $title, 'type_pesan' => $type],
            ]);
            
            return response()->json([
                'success' => false,
                'messege' => 'Pesan terkirim',
                'data' => $data,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'messege' => 'NIK anda tidak terdaftar'
            ], 200);
        }
    }
}
