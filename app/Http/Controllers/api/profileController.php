<?php

namespace App\Http\Controllers\api;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class profileController extends Controller
{
    public function getProfile(Request $request)
    {
        $id = $request->input('id');
        $data = DB::table('v_penduduk')->where('id', $id)->get();
        $dataPendukung = DB::table('pendukung')->where('id_penduduk', $id)->get();
        $dataInstansi = DB::table('data_instansi')->get();

        if ($data) {
            return response()->json([
                'success' => true,
                'data' => $data,
                'dataPendukung' => $dataPendukung,
                'dataInstansi' => $dataInstansi
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }
}
