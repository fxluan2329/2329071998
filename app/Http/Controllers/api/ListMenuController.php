<?php

namespace App\Http\Controllers\api;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomClass\FirebaseSendNotif;

class ListMenuController extends Controller
{
    public function listSurat()
    {
        $dataSurat = DB::table('data_surat')->get();

        if ($dataSurat) {
            return response()->json([
                'success' => true,
                'data' => $dataSurat
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }

    public function history(Request $request)
    {
        $id = $request->input('id');

        $dataHistori = DB::table('v_status_surat')->where('id_penduduk', $id)->get();

        if ($dataHistori) {
            return response()->json([
                'success' => true,
                'data' => $dataHistori
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }

    public function requestSurat(Request $request)
    {
        $id_penduduk = $request->input('id_penduduk');
        $id_surat = $request->input('id_surat');
        $keperluan = json_encode($request->input());

        $matchThese = ['id_penduduk' => $id_penduduk, 'konfirmasi_rt' => 0, 'konfirmasi_rw' => 0, 'konfirmasi_admin' => 0];
        $checkAntrian = DB::table('status_surat')->where($matchThese)->exists();

        if ($checkAntrian) {
            return response()->json([
                'success' => false,
                'message' => 'Mohon maaf anda masih memiliki pengajuan surat yang belum selesai',
            ], 200);
        } else {
            $insertData = DB::table('status_surat')->insert(
                ['id_surat' => $id_surat, 'id_penduduk' => $id_penduduk, 'keperluan' => $keperluan]
            );

            if ($insertData) {
                $this->sendNotif($id_penduduk);
                return response()->json([
                    'success' => true,
                    'message' => 'Pengajuan surat anda berhasil'
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Pengajuan surat anda gagal. Silahkan hubungi Customer Service kami'
                ], 200);
            }
        }
    }

    public function accSurat(Request $request){
        $id = $request->input('id');
        $jabatan = $request->input('jabatan');
        $accSurat = $request->input('accSurat');
        
        if($jabatan == 'Ketua RT'){
            $column = 'konfirmasi_rt';
        }
        
        if($jabatan == 'Ketua RW'){
            $column = 'konfirmasi_rw';
        }
        
        $update = DB::table('status_surat')
              ->where('id', $id)
              ->update([$column => $accSurat]);
              
         if ($update) {
            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }
}
