<?php

namespace App\Http\Controllers\api;

use App\penduduk;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomClass\FirebaseSendNotif;

class PesanMasukController extends Controller
{
    public function getPesan(Request $request)
    {
        $id = $request->input('id');

        $pesan = DB::table('pesan_masuk')->where('id_penduduk', $id)->orderBy('id', 'desc')->get();

        if ($pesan) {
            
            return response()->json([
                'success' => true,
                'data' => $pesan,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }
    
    public function deletePesan(Request $request){
        $id = $request->input('id');
        
        $delete = DB::table('pesan_masuk')->where('id', $id)->delete();
        
        if ($delete) {
            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
            ], 200);
        }
    }
    
    public function statusPesan(Request $request){
        $id = $request->input('id');
        
        $update = DB::table('pesan_masuk')
              ->where('id', $id)
              ->update(['status_pesan' => 1]);
              
        if ($update) {
            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'error' => $update
            ], 200);
        }
    }
}
