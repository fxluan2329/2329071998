<?php

namespace App\Http\Controllers;

use App\data_instansi;
use Illuminate\Http\Request;

class DataInstansiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\data_instansi  $data_instansi
     * @return \Illuminate\Http\Response
     */
    public function show(data_instansi $data_instansi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\data_instansi  $data_instansi
     * @return \Illuminate\Http\Response
     */
    public function edit(data_instansi $data_instansi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\data_instansi  $data_instansi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, data_instansi $data_instansi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\data_instansi  $data_instansi
     * @return \Illuminate\Http\Response
     */
    public function destroy(data_instansi $data_instansi)
    {
        //
    }
}
