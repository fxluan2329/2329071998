<?php

namespace App\Http\Controllers;

use App\pendukung;
use Illuminate\Http\Request;

class PendukungController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pendukung  $pendukung
     * @return \Illuminate\Http\Response
     */
    public function show(pendukung $pendukung)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pendukung  $pendukung
     * @return \Illuminate\Http\Response
     */
    public function edit(pendukung $pendukung)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pendukung  $pendukung
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pendukung $pendukung)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pendukung  $pendukung
     * @return \Illuminate\Http\Response
     */
    public function destroy(pendukung $pendukung)
    {
        //
    }
}
