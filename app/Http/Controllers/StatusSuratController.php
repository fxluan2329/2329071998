<?php

namespace App\Http\Controllers;

use App\status_surat;
use Illuminate\Http\Request;

class StatusSuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\status_surat  $status_surat
     * @return \Illuminate\Http\Response
     */
    public function show(status_surat $status_surat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\status_surat  $status_surat
     * @return \Illuminate\Http\Response
     */
    public function edit(status_surat $status_surat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\status_surat  $status_surat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, status_surat $status_surat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\status_surat  $status_surat
     * @return \Illuminate\Http\Response
     */
    public function destroy(status_surat $status_surat)
    {
        //
    }
}
